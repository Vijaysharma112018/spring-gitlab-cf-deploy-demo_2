FROM openjdk:8-jdk-alpine

# The application's jar file
ARG JAR_FILE=target/demo-0.0.1-SNAPSHOT.jar

# Add the application's jar to the container
ADD ${JAR_FILE} /usr/local/demo-0.0.1-SNAPSHOT.jar

# Run the jar file 
ENTRYPOINT ["java","-jar","/usr/local/demo-0.0.1-SNAPSHOT.jar"]

EXPOSE 8080